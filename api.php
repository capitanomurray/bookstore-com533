<?php
require 'Slim/Slim.php';
\Slim\ Slim::registerAutoloader();
use Slim\ Slim;
$app = new Slim();
$app->get('/books', 'getBooks'); //retrieve all books
$app->get('/books/:id','getBook'); //get single book based on id
$app->post('/books','addBook'); //post book
$app->put('/books/:id','updateBook'); //update book based on id
$app->delete('/books/:id','deleteBook'); //delete book based on id

$app->run();


//SERVER DB CONNECTION
// function getConnection() {
//   $dbhost="localhost";
//   $dbuser="B00580242";
//   $dbpass="yDjdPav4";
//   $dbname="b00580242";
//   $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
//   $dbh-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//   return $dbh;
// }

//LOCALHOST DB CONNECTION
function getConnection() {
  $dbhost="localhost";
  $dbuser="root";
  $dbpass="root";
  $dbname="com533";
  $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
  $dbh-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $dbh;
}

// REST GET ALL BOOKS
function getBooks() {
  $sql = "select * FROM book ORDER BY id ";
  try {
    $db = getConnection();
    $stmt = $db->query($sql);
    $books = $stmt->fetchAll(PDO::FETCH_OBJ);
    $db = null;
    responseJson('{"book":'.json_encode($books).'}',200);
    } catch(PDOException $e) {
      responseJson(' {"error": {"text": '.$e->getMessage().'}}', 500);
  }
}

//REST GET BASED ON ID
function getBook($id) {
  $sql = "SELECT * FROM book WHERE id=:id";
  try {
    $db = getConnection();
    $stmt = $db-> prepare($sql);
    $stmt-> bindParam("id", $id);
    $stmt-> execute();
    $book = $stmt-> fetchObject();
    $db = null;
    responseJson(json_encode($book), 200);
  } catch (PDOException $e) {
      responseJson('{"error":{"text":'.$e-> getMessage().'}}', 500);
  }
}

// //POST BOOK
function addBook() {
    $request = Slim::getInstance()-> request();
    $book = json_decode($request-> getBody());
    $sql ="INSERT INTO book(ISBN,author,title,publisher,year,bookImage)values(:ISBN,:author,:title,:publisher,:year,:bookImage)";
    try {
      $db = getConnection();
      $stmt = $db-> prepare($sql);
      $stmt-> bindParam("ISBN", $book-> ISBN);
      $stmt-> bindParam("author", $book-> author);
      $stmt-> bindParam("title", $book-> title);
      $stmt-> bindParam("publisher", $book-> publisher);
      $stmt-> bindParam("year", $book-> year);
      $stmt-> execute();
      $book-> id = $db-> lastInsertId();
      $db = null;
      responseJson(json_encode($book), 201);
    } catch (PDOException $e) {
        responseJson('{"error":{"text":'.$e-> getMessage().'}}', 500);
    }
}


//REST PUT BASED ON ID
function updateBook($id) {
    $request = Slim::getInstance()-> request();
    $body = $request-> getBody();
    $book = json_decode($body);
    $sql = "UPDATE book SET ISBN=:ISBN, author=:author, title=:title, publisher=:publisher, year=:year WHERE id=:id";
    try {
      $db = getConnection();
      $stmt = $db-> prepare($sql);
      $stmt-> bindParam("ISBN", $book-> ISBN);
      $stmt-> bindParam("author", $book-> author);
      $stmt-> bindParam("title", $book-> title);
      $stmt-> bindParam("publisher", $book-> publisher);
      $stmt-> bindParam("year", $book-> year);
      $stmt-> bindParam("id", $id);
      $stmt-> execute();
      $db = null;
        responseJson(json_encode($book), 200);
    } catch (PDOException $e) {
      responseJson('{"error":{"text":'.$e-> getMessage().'}}', 500);
  }
}

//REST DELETE BASED ON ID
function deleteBook($id) {
    $sql = "DELETE FROM book WHERE id=:id";
    try {
      $db = getConnection();
      $stmt = $db-> prepare($sql);
      $stmt-> bindParam("id", $id);
      $stmt-> execute();
      // $book = $stmt-> fetchObject();
      $db = null;
        responseJson(json_encode("Delete My Friend"), 200);
    } catch (PDOException $e) {
      responseJson('{"error":{"text":'.$e-> getMessage().'}}', 500);
  }
}

function responseJson($responseBody, $statusCode) {
  $app = Slim::getInstance();
  $response = $app-> response();
  $response['Content-­‐Type'] = 'application/json';
  $response-> status($statusCode);
  $response-> body($responseBody);
}




?>

