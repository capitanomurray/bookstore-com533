<!doctype html>
<html>
<?php require_once ("includes/head.php") ?>
    <body>
        <?php require_once ("includes/header.php") ?>
        <main>
            <div class="container">
              <div class="mainArea">
                <form id="bookSubmit" action="" method="POST">
                    <label>ISBN:</label> <input class="form-control" id="ISBN" name="ISBN" placeholder="13 digit ISBN number" type="text" required maxlength="13">
                    <label>Author:</label> <input class="form-control" id="author" name="author" placeholder="The author of the book" required type="text">
                    <label>Book title:</label> <input class="form-control" id="title" name="title" placeholder="The title of the book" required type="text">
                    <label>Publisher:</label> <input class="form-control" id="publisher" name="publisher" placeholder="The publisher of the book" required type="text">
                    <label>Year:</label> <input class="form-control" id="year" name="year" placeholder="The year the book was published" maxlength="4" required type="text">
                    <input class="btn btn-embossed btn-wide btn-primary" type="submit" id="btnSave">
                </form>
              </div><!-- end mainArea -->

                <!-- <div id="staffs">
                      <ul id="book_list"></ul>
                </div> -->

            </div><!-- end container -->
        </main>
        <?php require_once ("includes/footer.php") ?>

        <script src="js/w3.js"></script>
    </body>
</html>
