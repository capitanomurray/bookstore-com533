

describe("Spy on ajax call", function() {
    it("should make an AJAX request with correct setting", function() {
        spyOn($, "ajax");
        getBook(123, processResponse);
        expect($.ajax).toHaveBeenCalledWith({
            type: "GET",
            url: "books/" + 123,
            dataType: "json",
            success: processResponse
        });
    });
});