<!doctype html>
<html>
<?php require_once ("includes/head.php") ?>
    <body>
        <?php require_once ("includes/header.php") ?>
        <main>

            <script id="book_tmpl" type="text/x-jQuery-tmpl">
                <div class='col-sm-6 col-md-2'>
                    <div class='book-list'>
                        {{if bookImage.indexOf("http") > -1}}
                            <img width="100%" src='${bookImage}'>
                        {{else}}
                            <img width="100%" src="uploads/${bookImage}">
                        {{/if}}
                        <div class='caption'>
                            <h6 class='book-list-title'>${title}</h6>
                            <p class='book-list-content'>${author}</p>
                            <p class='book-list-content'>ISBN:${ISBN}</p>
                        </div>
                    </div>
                </div>
            </script>
            <div class="container">
                <div class="table">
                  <div class="table-responsive">
                    <table id="books" class="table table-striped table-hover">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>ISBN</th>
                          <th>Book title</th>
                          <th>Author</th>
                          <th>Publisher</th>
                          <th>Year</th>
                        </tr>
                      </thead>
                      <!-- <tbody id="book_list"> -->

                      <!-- </tbody> -->
                    </table>
                  </div><!-- end table-responsive -->
                </div><!-- end table container -->
            </div><!-- end container -->

            <div class="container-fluid">
                <div id="book_list" class="row">

                </div>
                <button type='button' class='btn btn-default' data-toggle='tooltip' data-placement='right' title='Tooltip on right'>Tooltip on right</button>
            </div>

    
        <?php require_once ("includes/delete-form.php") ?> 
            

        <div class="content-element">
            <div class="container">
                <div class="row">
                    <div class="text-center">
                         <h3>Update a record</h3>
                         <p>Enter the id of a book to delete its record.</p>
                    </div><!-- end text-center -->
                        <div>
                            <div class="col-md-2">
                                <input class="form-control" type="text" name="book_id2" id="book_id2" placeholder="The ID of the book" required>
                            </div><!-- end columns -->
                            <div class="col-md-2">
                                <input class="form-control" name="ISBN2" id="ISBN2" placeholder="13 digit ISBN number" type="text" maxlength="13">
                            </div><!-- end columns -->
                            <div class="col-md-2">
                                <input class="form-control" name="author2" id="author2" placeholder="Author" type="text">
                            </div><!-- end columns -->
                            <div class="col-md-2">
                                <input class="form-control" name="title2" id="title2" placeholder="Book title" type="text">
                            </div><!-- end columns -->
                            <div class="col-md-2">
                                <input class="form-control" name="publisher2" id="publisher2" placeholder="Publisher" type="text">
                            </div><!-- end columns -->
                            <div class="col-md-2">
                                <input class="form-control" name="year2" id="year2" placeholder="Published year" maxlength="4" type="text">
                            </div><!-- end columns -->
                            <div class="text-center">
                                <input type="hidden" name="_METHOD" value="PUT" />
                                <input id="btnUpdateBook" class="btn btn-embossed btn-wide btn-inverse" type="submit" value="Update">
                            </div><!-- end text-center -->
                        </div>
                </div><!-- end row  -->
            </div><!-- end container -->
        </div><!-- end content-element -->

        <div class="content-element-two silver">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4" style="text-align: center">
                        <div style="text-align: center"><img alt="" src="img/penguin_books_logo.png" width="20%"></div>
                        <h4 class="penguin-marg">#penguininstagram</h4>
                    </div>
                </div>

                <div class="row">
                    <div>
                        <div id="instafeed"></div>
                    </div>
                </div><!-- end row -->
                <br>
                <br>
                <br>
                <!-- search bar -->
                <form class="right">
                <input type="search" class="form-control input-lg" id="search" placeholder="Type in book title to search">
                </form>
                <div id="results"></div>
                <!-- end search bar -->
            </div><!-- end container -->
        </div><!-- end content-element -->

        </main>
        <?php require_once ("includes/footer.php") ?>

        <script type="text/javascript" src="js/instafeed.js"></script>
        <script>
            var feed = new Instafeed({
            get: 'tagged',
            tagName: 'penguininstagram',
            resolution: 'standard_resolution',
            limit: '3',
            clientId: '8314a33d4c894e63a78b964ae71aa55d',
            template: '<div class="col-md-4 column instagram"><a href="{{link}}"><img src="{{image}}" style="width: 100%" /></a></div>'
            });

            // run the feed
            feed.run();
        </script>


        <!-- AJAX POST BOOK -->
        <!-- <script src="js/postBook.js"></script> -->
        <!-- SHOW BOOKS -->
        <script src="js/showBooks.js"></script>
          <!-- AJAX DELETE BOOK -->
        <script src="js/deleteBook.js"></script>
          <!-- AJAX UPDATE BOOK -->
        <script src="js/updateBook.js"></script>
          <!-- SEARCH BOOKS -->
        <script src="js/search-books.js"></script>

    

    </body>
</html>