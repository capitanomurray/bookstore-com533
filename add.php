<!doctype html>
<html>
<?php require_once ("includes/head.php") ?>
    <body>
        <?php require_once ("includes/header.php") ?>
        <main>

            <div class="content-element silver">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="text-center">
                                <h3>Add a book</h3>
                                <p>Search for a book within the Google Books directory by entering the Title or Author.</p>
                                <div>
                                    <input type="text" name="search" id="ISBN_search" class="input-field form-control input-hg" placeholder="Title, Author..."  required>
                                    <button onclick="dosearch();" id="search-submit" class="btn btn-embossed btn-wide btn-lg btn-primary" name="submit">Search</button>
                                </div>

                            </div><!-- end text-center -->
                        </div><!-- end columns  -->
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end content-element -->

            <div class="container">
                <div class="result">


                </div><!-- end result -->

            </div><!-- ene container -->

        </main>
        <?php require_once ("includes/footer.php") ?>

        <script>
            function dosearch(){

            var ISBN = $('#ISBN_search').val();
            var url='https://www.googleapis.com/books/v1/volumes?q='+ISBN;

            $.getJSON(url,function(data){
                    $('.result').empty();
                    $.each(data.items, function(entryIndex, entry){
                        var html = '<div class="results well">';
                        //html += '<h3>' + entry.id + '</h3>';
                        html += '<h3>' + entry.volumeInfo.title + '</h3>';
                        html += '<div class="author">' + entry.volumeInfo.authors + '</div>';
                        html += '<div class="description">' + entry.volumeInfo.description + '</div>';
                        $('.result').append(html);
                    });

                    //here we send this query to database (thanks to AJAX):
                    //=====================================================
                    $.ajax({
                         type: 'POST',
                         url: 'addResult.php',
                         data: {
                             'ISBN' : ISBN
                             // 'title' : entry.volumeInfo.title,
                             // 'publisher' : entry.volumeInfo.subtitle||'not available',
                             // 'year' : entry.volumeInfo.authors[0]||'not available',
                             // 'author' : entry.volumeInfo.categories ||'not available',
                             // 'description' : entry.volumeInfo.description ||'not available'
                         },
                    });
                });

            }
        </script>



    </body>
</html>