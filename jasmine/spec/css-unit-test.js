describe('The default button', function(){
   
    beforeEach(function() {
        loadStyleFixtures('../../../../css/flat-ui.css');
        loadFixtures('../../../../templates/nav-template.php');
    });
    
    it('should have a color of white', function() {
      expect($('.navbar-btn').css('color')).toEqual('rgb(255, 255, 255)');
    });
    
    it('should have a background-color of green', function() {
      expect($('.navbar-btn').css('background-color')).toEqual('rgb(26, 188, 156)');
    });
});