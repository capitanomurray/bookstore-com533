describe('The delete form button', function(){
   
    beforeEach(function() {
        loadStyleFixtures('../../../../css/flat-ui.css');
        loadFixtures('../../../../includes/delete-form.php');
    });
    
    it('should have a color of white', function() {
      expect($('.btn-danger').css('color')).toEqual('rgb(255, 255, 255)');
    });
    
    it('should have a background-color of red', function() {
      expect($('.btn-danger').css('background-color')).toEqual('rgb(231, 76, 60)');
    });


    it('should have a type of submit', function() {
      expect($('.btn-danger').attr('type')).toEqual('submit');
    });
});