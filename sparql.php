<!doctype html>
<html>
<?php require_once ("includes/head.php") ?>
    <body>
        <?php require_once ("includes/header.php") ?>
        <main>
            <div class="container">

                <button onclick="retrieveData();">Execute</button>
                <div id="raw_output"></div>
                <div id="results"></div>
                <div id="output_div" class="output_div"></div>
            </div><!-- end container -->



        </main>
        <?php require_once ("includes/footer.php") ?>

        <script>
           function retrieveData() {
             var query = "PREFIX : <http://dbpedia.org/resource/>PREFIX dbp: <http://dbpedia.org/ontology/>PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>PREFIX dbpprop: <http://dbpedia.org/property/>SELECT ?food ?abstract ?thumbnail ?caption WHERE { ?food rdf:type dbp:Food ; dbpprop:name ?name ; dbpprop:caption ?caption ; dbp:abstract ?abstract . OPTIONAL { ?food dbp:thumbnail ?thumbnail } FILTER ( regex(?name, "Calzone" )) FILTER ( langMatches(lang(?abstract), "EN"))} ORDER BY ?food";

             var url = 'http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=' + encodeURIComponent(query) + '&output=json';

             $.ajax({
               url: url,
               dataType: "json",
               success: function (data) {
                 $('#results').show();
                 $('#raw_output').text(JSON.stringify(data, null, 3));
                 handle_json(data);
               },
               error: function(e) {}
             });
           }

           function handle_json(json) {
             $('#output_div').text("");

             $.each(
               json['results']['bindings'], function(index, value) {
                 var html = "";
                 name = value['food']['value'].replace("http://dbpedia.org/resource/", "");
                 name = decodeURIComponent(name.replace(/_/g, " "));
                 html += "<div><h3><b>" + name + ":</b> (" + value['caption']['value'] + ")</h3></div>";

                 if (value['thumbnail'] != null)
                   html += "<div class='inline thumb'><img style='width: 200px' src='" + value['thumbnail']['value'].replace("200px", "150px") + "'/></div>";
                 else
                   html += "<div class='inline thumb'><img src=''/></div>";

                 html += "<div class='inline abstract'>" + value['abstract']['value'] + "</div><div class='clear'></div><br>";

                 $('#output_div').append(html);
               }
             );
           }
        </script>






    </body>
</html>




