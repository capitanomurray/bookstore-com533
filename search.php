<!doctype html>
<html>
<?php require_once ("includes/head.php") ?>
    <body>
        <?php require_once ("includes/header.php") ?>
        <main>

            <div class="content-element silver">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="text-center">
                                <h3>Search for a book</h3>
                                <p>Search for a book within the Google Books directory by entering the Title or Author.</p>
                                <div>
                                    <input type="text" name="search" id="book-search" class="input-field form-control input-hg" placeholder="Title, Author..."  required>
                                    <button id="search-submit" class="btn btn-embossed btn-wide btn-lg btn-primary" type="button" autocomplete="off" data-loading-text="Searching...">Search</button>
                                </div>

                            </div><!-- end text-center -->
                        </div><!-- end columns  -->
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end content-element -->

            <div class="container">
                <div class="result">


                </div><!-- end result -->

            </div><!-- ene container -->

        </main>
        <?php require_once ("includes/footer.php") ?>
            <script src="js/readmore.js"></script>

        <script>
            $(document).ready(function() {
                $('#search-submit').click(function() {
                    var $btn = $(this).button('loading')
                    // business logic...
                    $btn.button('reset')
                    var data = $('#book-search').val(); //get isbn direct from input, no need for php
                    var url='https://www.googleapis.com/books/v1/volumes?q='+data;
                    $.getJSON(url,function(data){
                        $('.result').empty();
                        $.each(data.items, function(entryIndex, entry){


                            if (typeof entry.volumeInfo.industryIdentifiers != 'undefined') {

                                var html = '<div class="results well">';

                                try {
                                    html += '<img src="' + entry.volumeInfo.imageLinks.thumbnail + '">';    
                                } catch(e) {
                                    html += '<img src="http://placehold.it/130x200&text=Image+Not+Found">';    
                                }
                                
                                html += '<h3>' + entry.volumeInfo.title + '</h3>';
                                html += '<p>' + entry.volumeInfo.authors + '</p>';
                                html += '<div class="result-desription"><p>' + entry.volumeInfo.description + '</p></div>';
                                html += '<p>Publisher: ' + entry.volumeInfo.publisher + '</p>';
                                html += '<p>Date published: ' + entry.volumeInfo.publishedDate + '</p>';
                                html += '<p>ISBN: ' + entry.volumeInfo.industryIdentifiers[0].identifier + '</p>';

                                html += '<a href="'+ entry.volumeInfo.infoLink +'">More info</a>';



                                html += '<form method="post">';
                                html += '   <input type="hidden" class="formISBN" name="isbn" value="' + entry.volumeInfo.industryIdentifiers[0].identifier + '">';
                                

                                try { 
                                    html += '   <input type="hidden" class="formBookImage" name="bookImage" value="' + entry.volumeInfo.imageLinks.thumbnail + '">';
                                } catch(e) {
                                    html += '   <input type="hidden" class="formBookImage" name="bookImage" value="http://placehold.it/130x200&text=Image+Not+Found">';
                                }
                                html += '   <input type="hidden" class="formAuthor" name="author" value="' + entry.volumeInfo.authors + '">';
                                html += '   <input type="hidden" class="formTitle" name="title" value="' + entry.volumeInfo.title + '">';
                                html += '   <input type="hidden" class="formPublisher" name="publisher" value="' + entry.volumeInfo.publisher + '">';
                                html += '   <input type="hidden" class="formYear" name="year" value="' + entry.volumeInfo.publishedDate + '">';
                                html += '</form>';


                                html += '<button id="post-result" class="submit-book btn btn-primary">Submit</button>';
                                $('.result').append(html);

                            }

                        });

                    $('.result-desription').readmore({
                        speed: 500,
                        moreLink: '<a href="#">Read more</a>'
                    });

                    $('.submit-book').click(function() {
                        swal("Success!", "The book has been added", "success") //run sweet alert if successful

                        var isbn = $(this).parent().find('form').find('.formISBN').val();
                        var bookimage = $(this).parent().find('form').find('.formBookImage').val();
                        var title = $(this).parent().find('form').find('.formTitle').val();
                        var publisher = $(this).parent().find('form').find('.formPublisher').val();
                        var year = $(this).parent().find('form').find('.formYear').val();
                        var author = $(this).parent().find('form').find('.formAuthor').val();

                        $.post( "addResult.php", { isbn: isbn, title: title, publisher: publisher, year: year, author: author, bookimage: bookimage } );
                    });

                    });
                });

            });
        </script>


    </body>
</html>