<footer>
  <!-- JQUERY -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <!-- JQUERY TMPL -->
  <script src="https://my-web-js.googlecode.com/files/jquery.tmpl.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <!-- FLAT UI JS-->
  <script src="js/flat-ui.min.js"></script>
  <!-- JQUERY VALIDATE -->
  <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
  <!-- REMODAL -->
  <script src="js/jquery.remodal.min.js"></script>
  <!-- AJAX POST BOOK -->
  <script src="js/postBook.js"></script>
  <!-- FUNCTIONS -->
  <script src="js/functions.js"></script>
  <!-- SWEET ALERT -->
  <script src="js/sweet-alert.min.js"></script>
  <link rel="stylesheet" href="css/sweet-alert.css">

  
</footer>
