<?php

function post_curl($_url, $_data) {
   $mfields = '';
   foreach($_data as $key => $val) {
      $mfields .= $key . '=' . $val . '&';
   }
   rtrim($mfields, '&');
   $pst = curl_init();

   curl_setopt($pst, CURLOPT_URL, $_url);
   curl_setopt($pst, CURLOPT_POST, count($_data));
   curl_setopt($pst, CURLOPT_POSTFIELDS, $mfields);
   curl_setopt($pst, CURLOPT_RETURNTRANSFER, 1);

   $res = curl_exec($pst);

   curl_close($pst);
   return $res;
}

function post_to_url($url, $data) {
   $fields = '';
   foreach($data as $key => $value) {
      $fields .= $key . '=' . $value . '&';
   }
   rtrim($fields, '&');
   $post = curl_init();

   curl_setopt($post, CURLOPT_URL, $url);
   curl_setopt($post, CURLOPT_POST, count($data));
   curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
   curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);

   $result = curl_exec($post);

   curl_close($post);
   return $result;
}

 ?>

