<div class="content-element silver">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="text-center">
                                <h3>Delete a record</h3>
                                <p>Enter the id of a book to delete its record.</p>
                                <div>
                                    <input class="form-control" type="text" name="id" id="deleteID" placeholder="The ID of the book" required>
                                    <input type="hidden" name="_METHOD" value="DELETE" />
                                    <input id="btnDeleteBook" class="btn btn-embossed btn-wide btn-danger" type="submit" value="Delete">
                                </div>
                            </div><!-- end text-center -->
                        </div><!-- end columns  -->
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end content-element -->