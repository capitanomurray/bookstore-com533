<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BookDB</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/flat-ui.min.css">
    <link rel="stylesheet" href="css/jquery.remodal.css">
    <link rel="stylesheet" href="css/style.css">

    <link rel="author" href="humans.txt">
    <link rel="shortcut icon" href="img/favicon.ico">
</head>