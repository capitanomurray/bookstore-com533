<div class="remodal" data-remodal-id="modal" data-remodal-options="hashTracking: false">
  <h2>Submit a book</h2>
  <form action="insert-book.php" method="POST" enctype="multipart/form-data" class="clearfix">
    <label>ISBN:</label> <input class="form-control" id="ISBN" name="ISBN" placeholder="13 digit ISBN number" type="text" required maxlength="13">
    <label>Author:</label> <input class="form-control" id="author" name="author" placeholder="The author of the book" required type="text">
    <label>Book title:</label> <input class="form-control" id="title" name="title" placeholder="The title of the book" required type="text">
    <label>Publisher:</label> <input class="form-control" id="publisher" name="publisher" placeholder="The publisher of the book" required type="text">
    <label>Year:</label> <input class="form-control" id="year" name="year" placeholder="The year the book was published" maxlength="4" required type="text">
    <label>Book Image:</label> <input class="form-control" id="bookImage" name="bookImage" placeholder="The image cover of the book"  required type="file">
    <input type="submit" value="submit">
  </form>
</div>