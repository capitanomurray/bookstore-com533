<header>
  <nav class="navbar navbar-inverse " role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
        <span class="sr-only">Toggle navigation</span>
      </button>
      <a class="navbar-brand" href="#">Murrays Bookstore</a>
    </div>
    <div class="collapse navbar-collapse" id="navbar-collapse-01">
      <ul class="nav navbar-nav">
        <li class="active"><a href="books.php">Books</a></li>
        <li><a href="search.php">Search for books</a></li>
        <li><a href="add.php">Add books by ISBN</a></li>

      </ul>
      <div class="navbar-right">
        <a data-remodal-target="modal" class="btn btn-default navbar-btn">Submit a book</a>
      </div>
    </div><!-- /.navbar-collapse -->
  </nav><!-- /navbar -->
</header>
<?php require_once("includes/functions.php") ?>
<?php require_once("book-submit-modal.php") ?>

