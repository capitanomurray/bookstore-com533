function showError() {
  swal("Oops!", "Something went wrong there", "error") //run sweet alert if successful
}

function showResponse(responseData) {
    alert("Your book has been submitted.");
}

function Book(ISBN, author, title, publisher, year) {
    this.ISBN = ISBN;
    this.author = author;
    this.title = title;
    this.publisher = publisher;
    this.year = year;
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

