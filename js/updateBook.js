$(document).ready(function() {
    $('#btnUpdateBook').click(function(){

              var id = $("#book_id2").val();
              var ISBN = $("#ISBN2").val();
              var author = $("#author2").val();
              var title = $("#title2").val();
              var publisher = $("#publisher2").val();
              var year = $("#year2").val();

        var dataObject = {'id': id, 'ISBN': ISBN, 'author': author, 'title': title, 'publisher': publisher, 'year': year};

        $.ajax({
                  url: 'api.php/books/' + id,
                  type: 'PUT',
                  data: JSON.stringify(dataObject),
                  contentType: 'application/json',
                  success: function(response) {
                    swal("Success!", "The record was updated", "success") //run sweet alert if successful

                    $(document).ready(function() {
                          $.ajax({
                            type: 'GET',
                            dataType:"json",
                            url:"api.php/books",
                            success: showAllBooks,
                            error: showError
                          });
                    });


                    //SHOW ALL BOOKS WITH TMPL
                    function showAllBooks(responseData) {
                        $("#book_list").html('');
                        $("#book_tmpl").tmpl(responseData.book).appendTo("#book_list");
                        $("#book_id2").val('');
                        $("#ISBN2").val('');
                        $("#author2").val('');
                        $("#title2").val('');
                        $("#publisher2").val('');
                        $("#year2").val('');
                    }

                    }
                });
    });
});