// //AJAX GET
$(document).ready(function() {
  $.ajax({
    type: 'GET',
    dataType:"json",
    url:"api.php/books",
    success: showAllBooks,
    error: showError
  });
});


//SHOW ALL BOOKS WITH TMPL
function showAllBooks(responseData) {
  $("#book_tmpl").tmpl(responseData.book).appendTo("#book_list");
}