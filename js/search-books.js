$(function () {

  $('#search').keyup(function(){ //when key is pressed in search bar
      var searchField = $('#search').val(); //get the value of search input and store in variable
      var myExp = new RegExp(searchField, "i"); //do a case-insensitive search
      var output = '<div class="row"><h2>Search Results</h2>';
      var count = 1;

      $.getJSON('api.php/books', function(data) { //retrieve the JSON file
        $.each(data.book, function(key, val){ //loop each key/value in JSON file
          if ((val.title.search(myExp) !== -1)) { //if a keypress character represents value in title and/or channel data in json file then output following functions
            output += '<div class="col-md-6 well">';
            output += '<div class="col-md-3"><img class="img-responsive" src="'+ val.bookImage +'" alt="'+ val.title +'" /></div>';
            output += '<div class="col-md-7">';
            output += '<h4 class="title">' + val.title + '</h4>';
            output += '<p>Publisher: ' + val.publisher + '</p>';
            output += '<p>Date released: ' + val.year + ' on ' + val.date + '</p>';
            output += '</div>';
            output += '</div>';
            if(count%2 === 0){
              output += '</div><div class="row">';
            }
            count++;
          }
        }); //end each
        output += '</div>';
        $('#results').html(output); //output result to the #results div
      });
  });

});

