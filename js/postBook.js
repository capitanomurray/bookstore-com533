

// //AJAX GET
// $(document).ready(function() {
//   $.ajax({
//     type: 'GET',
//     dataType:"json",
//     url:"api.php/books",
//     success: showAllBooks,
//     error: showError
//   });
// });

// $('#bookSubmit').validate({


//AJAX POST
$(document).ready(function() {
    $('#bookSubmit').validate({
        errorClass: "alert alert-danger",
        submitHandler: function(form) {
            var book = new Book($("#ISBN").val(), $("#author").val(), $("#title").val(), $("#publisher").val(), $("#year").val());
            var inst = $.remodal.lookup[$('[data-remodal-id=modal]').data('remodal')];
            $.ajax({
              type: 'POST',
              dataType:"json",
              url:"api.php/books",
              data:JSON.stringify(book),
              success: function(){
                inst.close();//close the modal
              },
              error: showError,
              complete: function(){
                    $("input[type='text'").val('');//clear inputs when complete
                    window.location.reload(true); //refresh page
                }
            });
        }
    });
});

function Book(ISBN, author, title, publisher, year) {
    this.ISBN = ISBN;
    this.author = author;
    this.title = title;
    this.publisher = publisher;
    this.year = year;
}



//SHOW ALL BOOKS
// function showAllBooks(responseData) {
//   $.each(responseData.book,function(index,book){
//     $("#book_list").append("<li type='square'>Id:"+book.id+",ISBN:"+book.ISBN+",Author:"+book.author+",Title:"+book.title+",Publisher:"+book.publisher+",Year:"+book.year);
//       $("#book_list").append("</li>");
//     });
// }

//SHOW ALL BOOKS WITH TMPL
// function showAllBooks(responseData) {
//   $("#book_tmpl").tmpl(responseData.book).appendTo("#book_list");
// }


//CROSS DOMAIN REQUEST
// $(document).ready(function() {
//     $("#suggestion").keyup(function(e) {
//         suggestionUrl = "https://en.wikipedia.org/w/api.php?action=opensearch&format=json&search="+$("#suggestion").val();
//         $.ajax({
//             type: 'GET',
//             dataType: "jsonp",
//             url: suggestionUrl,
//             success: function(responseData) {
//                 $("#suggestion_list").empty();
//                 $.each(responseData[1], function(
//                     key, value) {
//                     $("#suggestion_list").append("<li> <a href="+responseData[3][key]+">"+value+"</a></li>");
//                 });
//             },
//             error: function() {
//                 $("#suggestion_list").empty();
//                 alert('Failed to make cross domain request');
//             }
//         });
//     });
// });
