// //AJAX DELETE
$(document).ready(function() {
  $('#btnDeleteBook').click(function(){

    var id = $("#deleteID").val();

      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      }, function() {
          $.ajax({
            url: 'api.php/books/' + id,
            type: 'DELETE',
            success: function(response) {
              swal("Success!", "The record was deleted", "success") //run sweet alert if successful
                $(document).ready(function() {
                  $.ajax({
                    type: 'GET',
                    dataType:"json",
                    url:"api.php/books",
                    success: showAllBooks,
                    error: showError
                  });
                });

                //SHOW ALL BOOKS WITH TMPL
                function showAllBooks(responseData) {
                  $("#book_list").html('');
                  $("#book_tmpl").tmpl(responseData.book).appendTo("#book_list");
                  $("#deleteID").val('');
                }
              }
          });
      });
    });
});